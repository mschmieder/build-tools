# Cross Platform Build-Tools #

this project focuses on providing common build tools and wrappers that will make your live easier when developing cross platform projects. The project makes use of a large number of open-source tools and compilers.

## Build System

you can use the build tools directly or using the CI Build Wrappers that ensure compliance of the VMs and also add docker support.

Building your projects directly use use the **cmakew** script available in the root folder of the build-tools.

This gives you a number of options that should be self-explanatory

```bash
cmakew [-h] [-a TARGET_ARCHITECTURE] -b BUILD_TYPE -c COMPILER
              [-g CMAKE_GENERATOR] [-i INSTALL_DIRECTORY] [-j NUM_THREADS]
              [-l LINK_TYPE] [-m MAKE_PROGRAM] -s SOURCE_DIRECTORY
              [-o BUILD_DIRECTORY] [-t TARGET_SYSTEM] [--clang-cl CLANG_CL]
              [--clean] [--build] [--no-configure] [--target TARGET]
              [--install] [--test]
              [--test-output-directory TEST_OUTPUT_DIRECTORY] [--analyze]
              [--analyzer LIST_ANALYZER]
              [--analyze-output-directory ANALYZE_OUTPUT_DIRECTORY]
              [--add-clang-checker LIST_CLANG_CHECKER] [--profile]
              [--coverage] [--coverage-tool LST_COVERAGE_TOOLS]
              [--coverage-output-directory COVERAGE_OUTPUT_DIRECTORY]
              [--gcovr-output-formatter GCOVR_OUTPUT_FORMATTER]
              [--toolchain-directory TOOLCHAIN_DIRECTORY]
              [--android-native-api-level ANDROID_NATIVE_API_LEVEL]
              [--android-platform ANDROID_PLATFORM]
              [--android-stl ANDROID_STL] [--verbose] [--cxxflags CXXFLAGS]
              [--cflags CFLAGS] [--ldflags LDFLAGS] [--log-level LOG_LEVEL]
              [--start-ide]
```


A simple project build would look like this:

```bash
# build travis using native build environment
python3 cmakew --source-directory=<SOURCE_DIR> \
               --build-directory=<BUILD_DIR> \
               --target-system=linux \
               --compiler=clang-3.9 \
               --target-architecture=x86_64 \
               --build-type=release \
               --link-type=shared \
               --build \
               --test \
               --install \
               --install-directory=<INSTALL_DIR> \
               --coverage \
               --coverage-output-directory=<BUILD_DIR>/metrics/coverage \
               --analyze \
               --add-analyzer=cppcheck \
               --analyze-output-directory=<BUILD_DIR>/metrics/ \
```


### Docker Support

If you have **docker** installed you can make use of **cmakew_docker** that will automatically execute **cmakew** within your docker container.

Just replace **cmakew** with **cmakew_docker** and add the "--docker-image" and "--docker-image-tag" parameter


## CI Build Wrappers

the project brings allows you to use an advanced build system that is based on CMake but supports multiple compilers as well as target systems by default. This build system also allows you to easily integrage your projects with **Travis CI** but also enables you for easy local builds

There is two modes that are currently available:

  * Native Build Environment
    - Linux
    - MacOS
    - Windows / Cygwin
  * Docker Build Environment
    - Linux

The **Native Build Environment** will use your host system in order to build the your project. Make sure you have all required tools and libarries available.

**Requirements**

   * bash (>=4.0 for travis builds)
   * CMake
   * python3 (make sure a python3 symlink is available in your system path)

To build the your project simply use the build wrapper that is located within the **ci/travis** subdirectory

```bash
# build travis using native build environment
# make sure necessary compilers and tools are available in system path
travis/ci/build.sh "your_project_name" native \
                    --source-directory=<SOURCE_DIR> \
                    --build-directory=<BUILD_DIR> \
                    --clean \
                    --target-system=linux \
                    --compiler=clang-3.8 \
                    --target-architecture=x86_64 \
                    --build-type=release \
                    --link-type=shared \
                    --build \
                    --test \
                    --install \
                    --install-directory=<BUILD_DIR>/install \
                    --num-threads=4
```

### System Setup - OSX

If you are leveraging travis OSX images you will need to bootstrap them with the right tools in order to build successfully. Use the **install_osx.sh** script within the **ci/travis/** folder to provision your system with the tools you need.

ATTENTION: **brew** will need to be installed first

### Code Coverage

The **Advanced Build System** bring support for code coverage using **gcov**. To enable the code coverage report generation simply add "**--coverage**" and optionally "**--coverage-outout-directory**" as parameters

```bash
# build travis using native build environment
# make sure necessary compilers and tools are available in system path
travis/ci/build.sh "your_project_name" native \
                    --source-directory=<SOURCE_DIR> \
                    --build-directory=<BUILD_DIR> \
                    --clean \
                    --target-system=linux \
                    --compiler=clang-3.8 \
                    --target-architecture=x86_64 \
                    --build-type=release \
                    --link-type=shared \
                    --build \
                    --test \
                    --install \
                    --install-directory=<BUILD_DIR>/install \
                    --num-threads=4 \
                    --coverage \
                    --coverage-output-directory=<BUILD_DIR>/metrics/coverage
```

Make sure you have "**--test**" enabled so that runtime coverage is available

**Attention**
Coverage is only available for **clang** and **gcc** compilers

### Static Code Analysis

The build system supports the creation of **scan-build** checks if a clang compiler is available, also **cppcheck** can be used for static code analyzation.

If a clang build is started just provied "**--analyse**" and optionally "**--analyze-output-directory**" to define where the results should be stored

```bash
# build travis using native build environment
# make sure necessary compilers and tools are available in system path
travis/ci/build.sh "your_project_name" native \
                    --source-directory=<SOURCE_DIR> \
                    --build-directory=<BUILD_DIR> \
                    --clean \
                    --target-system=linux \
                    --compiler=clang-3.8 \
                    --target-architecture=x86_64 \
                    --build-type=release \
                    --link-type=shared \
                    --build \
                    --test \
                    --install \
                    --install-directory=<BUILD_DIR>/install \
                    --num-threads=4 \
                    --coverage \
                    --coverage-output-directory=<BUILD_DIR>/metrics/coverage \
                    --analyze \
                    --analyze-output-directory=<BUILD_DIR>/metrics
```

To enable **cppcheck** add "**--add-analyzer cppecheck**" as parameter.

**Attention:**
Make sure that cppcheck is installed on your host system

```bash
# build travis using native build environment
# make sure necessary compilers and tools are available in system path
travis/ci/build.sh "your_project_name" native \
                    --source-directory=<SOURCE_DIR> \
                    --build-directory=<BUILD_DIR> \
                    --clean \
                    --target-system=linux \
                    --compiler=clang-3.8 \
                    --target-architecture=x86_64 \
                    --build-type=release \
                    --link-type=shared \
                    --build \
                    --test \
                    --install \
                    --install-directory=<BUILD_DIR>/install \
                    --num-threads=4 \
                    --coverage \
                    --coverage-output-directory=<BUILD_DIR>/metrics/coverage \
                    --analyze \
                    --add-analyzer cppecheck \
                    --analyze-output-directory=<BUILD_DIR>/metrics
```

### Docker Build System

Using modern docker technology, it is not necessary anymore to run the build on your host system directly. You can easily compiler for several Linux disributiobns using your Mac or Linux machine.

You can use the exact same commands as seen above, you'll just need to switch to the "**docker**" system instead of "**native**" 

We currently support an Ubuntu 17.04 build environment out of the box providing the following compilers and tools

**Ubuntu 17.04 Build System**

   * valgrind 
   * cmake 
   * gcc-4.8 
   * gcc-4.9 
   * gcc-5
   * gcc-6 
   * clang-3.7 
   * clang-3.8 
   * clang-3.9 
   * clang-4.0 
   * cppcheck 
   * scan-build
  
```bash
# build travis using native build environment
# make sure necessary compilers and tools are available in system path
travis/ci/build.sh "your_project_name" "docker-ubuntu-17.04" \
                    --source-directory=<SOURCE_DIR> \
                    --build-directory=<BUILD_DIR> \
                    --clean \
                    --target-system=linux \
                    --compiler=clang-3.8 \
                    --target-architecture=x86_64 \
                    --build-type=release \
                    --link-type=shared \
                    --build \
                    --test \
                    --install \
                    --install-directory=<BUILD_DIR>/install \
                    --num-threads=4 \
                    --coverage \
                    --coverage-output-directory=<BUILD_DIR>/metrics/coverage \
                    --analyze \
                    --add-analyzer cppecheck \
                    --analyze-output-directory=<BUILD_DIR>/metrics
```

## Directory Structure
