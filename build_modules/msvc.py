# -*- coding: utf-8 -*-
#
#  Copyright (c) 2017 Matthias Schmieder
#  This file is part of build-tools (https://bitbucket.org/mschmieder/build-tools).
#
#  This file is licensed to you under your choice of the GNU Lesser
#  General Public License, version 3 or any later version (LGPLv3 or
#  later), or the GNU General Public License, version 2 (GPLv2), in all
#  cases as published by the Free Software Foundation.
#

import shutil
import re
import sys
import os
import logging

if sys.platform.startswith("win"):
    import winreg
from .build_utils import *


##
## @brief      Class for msvc.
##
class Msvc(object):
    def __init__(self, compiler_id,
                 arch,
                 cmake_generator = None,
                 logger=None):

        self.__compiler_id = compiler_id
        self.__arch = arch
        self.__cmake_generator = cmake_generator
        self.__logger = logger

    ##
    ## @brief      Gets the version.
    ##
    ## @param      self  The object
    ##
    ## @return     The version.
    ##
    def get_version(self):
        return re.sub(r'[a-zA-Z-]+', '', self.__compiler_id)

    ##
    ## @brief      looks for the installation directory of Visual Studio
    ##
    ## @param      self  The object
    ##
    ## @return     path to visual studio
    ##
    def _find_install_dir(self):
        key_dir = r'SOFTWARE\Wow6432Node\Microsoft\VisualStudio\SxS\VS7'
        reg = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, key_dir, 0, winreg.KEY_WOW64_64KEY+winreg.KEY_READ)
        vs_dir = list(winreg.QueryValueEx(reg, self.get_version() + ".0"))[0]
       
        if not vs_dir:
            raise Exception("Could not find Visual Studio Common Tools. Please Check your installation!")

        return os.path.normpath(vs_dir)

    ##
    ## @brief      Gets the msvc architecture identifier.
    ##
    ## @param      self  The object
    ##
    ## @return     The msvc architecture identifier.
    ##
    def _get_msvc_architecture_id(self):
        if "x86_64" == self.__arch:
            return "x86_amd64"
        elif "x86" == self.__arch:
            return "amd64_x86"
        else:
            raise Exception("Unsupported architecture {0}".format(self.__arch))

    ##
    ## @brief      Gets the cxx compiler.
    ##
    ## @param      self  The object
    ##
    ## @return     The cxx compiler.
    ##
    def get_cxx_compiler(self):
        # TODO: add support for Visual Studio 2017. Compilers are stored on a different location
        if int(self.get_version()) < 15:
            vs_install_dir = self._find_install_dir()
            arch = self._get_msvc_architecture_id()
            return os.path.join(vs_install_dir, "VC", "BIN", arch, "cl.exe")
        else:
            # assume vcvarsall.bat was loaded previously
            return "cl.exe"
        
    ##
    ## @brief      Gets the cc compiler.
    ##
    ## @param      self  The object
    ##
    ## @return     The cc compiler.
    ##
    def get_cc_compiler(self):
        return self.get_cxx_compiler()

    ##
    ## @brief      Gets the gcov command.
    ##
    ## @param      self  The object
    ##
    ## @return     The gcov command.
    ##
    def get_gcov_command(self):
        return None

    ##
    ## @brief      Gets the pre configuration commands.
    ##
    ## @param      self  The object
    ##
    ## @return     The pre configuration commands.
    ##
    def get_pre_configuration_commands(self):
        install_dir = self._find_install_dir()
        arch = self._get_msvc_architecture_id()

        vcvarsall_locations = [os.path.join(str(install_dir), "VC", "vcvarsall.bat"),
                               os.path.join(str(install_dir), "VC/Auxiliary/Build", "vcvarsall.bat")]

        for loc in vcvarsall_locations:
            if os.path.exists(loc):
                return [loc, arch]

        return []

    ##
    ## @brief      Gets the pre build commands.
    ##
    ## @param      self  The object
    ##
    ## @return     The pre build commands.
    ##
    def get_pre_build_commands(self):
        return self.get_pre_configuration_commands()

    ##
    ## @brief      Gets the default cmake generator.
    ##
    ## @param      self  The object
    ##
    ## @return     The default cmake generator.
    ##
    def get_default_cmake_generator(self):
        if self.__cmake_generator is None:
            arch_postfix = ""
            if self.__arch == "x86_64":
               arch_postfix = "Win64"
            
            self.__cmake_generator = "Visual Studio {0} {1}".format(self.get_version(), arch_postfix).strip()

        return self.__cmake_generator

    ##
    ## @brief      Gets the additional cmake options.
    ##
    ## @param      self  The object
    ##
    ## @return     The additional cmake options.
    ##
    def get_additional_cmake_options(self):
        lst_options = []
        return lst_options

    ##
    ## @brief      Gets the cxx flags.
    ##
    ## @param      self  The object
    ##
    ## @return     The cxx flags.
    ##
    def get_cxx_flags(self):
        return ""

    ##
    ## @brief      Gets the c flags.
    ##
    ## @param      self  The object
    ##
    ## @return     The c flags.
    ##
    def get_c_flags(self):
        return self.get_cxx_flags()

    ##
    ## @brief      Gets the ld flags.
    ##
    ## @param      self  The object
    ##
    ## @return     The ld flags.
    ##
    def get_ld_flags(self):
        return ""

    ##
    ## @brief      Gets the cmake options.
    ##
    ## @param      self  The object
    ##
    ## @return     The cmake options.
    ##
    def get_cmake_options(self):
        opts = []
        if "Makefiles" in self.get_cmake_generator():
            opts = ["-DCMAKE_CXX_COMPILER:STRING={0}".format(self.get_cxx_compiler()).replace('\\', '/')]
            # check if we find a multi processor make replacement
            jom_exec = shutil.which("jom")
            if jom_exec is not None and jom_exec != "":
                opts.append("-DCMAKE_MAKE_PROGRAM:STRING={0}".format(jom_exec.replace('\\', '/')))
     
        return opts
