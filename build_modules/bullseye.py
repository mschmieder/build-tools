# -*- coding: utf-8 -*-
#
#  Copyright (c) 2017 Matthias Schmieder
#  This file is part of build-tools (https://bitbucket.org/mschmieder/build-tools).
#
#  This file is licensed to you under your choice of the GNU Lesser
#  General Public License, version 3 or any later version (LGPLv3 or
#  later), or the GNU General Public License, version 2 (GPLv2), in all
#  cases as published by the Free Software Foundation.
#

import re
import logging 
from .build_utils import *

if sys.platform.startswith("win"):
    from winreg import *

##
## @brief      Class for bullseye code coverage tool
##
class Bullseye(object):
    def __init__(self, output_dir,
                       logger = None):

        self.__output_dir = output_dir
        self.__logger = logger
        self.__bullseye_root = None

    ##
    ## @brief      Gets the name.
    ##
    ## @param      self  The object
    ##
    ## @return     The name.
    ##
    def get_name(self):
        return 'bullseye'


    ##
    ## @brief      Determines if installed.
    ##
    ## @param      self  The object
    ##
    ## @return     True if installed, False otherwise.
    ##
    def is_installed(self):
        if self.__bullseye_root is None:
            self.__bullseye_root = self.get_install_path()
      
        if self.__bullseye_root is None:
            return False
      
        return True


    ##
    ## @brief      enables bullseye coverage (globally!)
    ##
    ## @param      self  The object
    ##
    ## @return     None
    ##
    def enable(self):
        if self.is_installed():
            os.makedirs(self.__output_dir, exist_ok=True)
            build_utils.execute([os.path.join(self.__bullseye_root, "cov01.exe"), "--on"] )


    ##
    ## @brief      disables bullseye coverage
    ##
    ## @param      self  The object
    ##
    ## @return     None
    ##
    def disable(self):
        if self.is_installed():
            build_utils.execute([os.path.join(self.__bullseye_root, "cov01.exe"), "--off"])


    ##
    ## @brief      Gets the install path.
    ##
    ## @param      self  The object
    ##
    ## @return     The install path.
    ##
    def get_install_path(self):
        candidates = ["cov01"]

        bullseye_root = build_utils.find_executable(candidates)
        if bullseye_root is None:
            if sys.platform.startswith("win"):
                bullseye_root = self.__find_bullseye()
            else:
                self.__logger.debug("Could not locate bullseye! Check your installation")
        else:
            bullseye_root = os.path.dirname(bullseye_root)

        return bullseye_root


    ##
    ## @brief      Gets the environment.
    ##
    ## @param      self  The object
    ##
    ## @return     The environment.
    ##
    def get_env(self):
        env = dict()
        covfile = os.path.join(self.__output_dir, 'bullseye.cov')
        env['COVFILE'] = covfile
        return env


    ##
    ## @brief      search for bullseye on a windows system
    ##
    ## @param      self  The object
    ##
    ## @return     path to bullseye
    ##
    def __find_bullseye(self):
        keys = ["SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\BullseyeCoverage",
                "SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\BullseyeCoverage"]
        for key in keys:
            try:
                aReg = ConnectRegistry(None, HKEY_LOCAL_MACHINE)
                aKey = OpenKey(aReg, key)
                val = QueryValueEx(aKey, "InstallLocation")
                if val:
                    bullseye_path = val[0]

                if '64bit' == platform.architecture()[0]:
                    bullseye_path = os.path.join(self._bullseye_path, "bin/x64")
                else:
                    bullseye_path = os.path.join(self._bullseye_path, "bin/x86")

                if os.path.exists(bullseye_path):
                    return bullseye_path

            except EnvironmentError:
                _, err, _ = sys.exc_info()

        return None

