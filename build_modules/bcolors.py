# -*- coding: utf-8 -*-
#
#  Copyright (c) 2017 Matthias Schmieder
#  This file is part of build-tools (https://bitbucket.org/mschmieder/build-tools).
#
#  This file is licensed to you under your choice of the GNU Lesser
#  General Public License, version 3 or any later version (LGPLv3 or
#  later), or the GNU General Public License, version 2 (GPLv2), in all
#  cases as published by the Free Software Foundation.
#
import sys

if sys.platform.startswith("win"):
  ##
  ## @brief      Class for bcolors.
  ##             Windows shells to not support these codes
  ##  
  class bcolors:
      HEADER = ''
      OKBLUE = ''
      OKGREEN = ''
      WARNING = ''
      FAIL = ''
      ENDC = ''
      BOLD = ''
      UNDERLINE = ''
else:
  class bcolors:
        HEADER = '\033[95m'
        OKBLUE = '\033[94m'
        OKGREEN = '\033[92m'
        WARNING = '\033[93m'
        FAIL = '\033[91m'
        ENDC = '\033[0m'
        BOLD = '\033[1m'
        UNDERLINE = '\033[4m'
