# -*- coding: utf-8 -*-
#
#  Copyright (c) 2017 Matthias Schmieder
#  This file is part of build-tools (https://bitbucket.org/mschmieder/build-tools).
#
#  This file is licensed to you under your choice of the GNU Lesser
#  General Public License, version 3 or any later version (LGPLv3 or
#  later), or the GNU General Public License, version 2 (GPLv2), in all
#  cases as published by the Free Software Foundation.
#

import subprocess
import sys
import os
import re
import logging

from .bcolors import bcolors

##
## @brief      Class for build shell that is being used to run build commands
##
class BuildShell(object):
    def __init__(self, env=os.environ.copy(), cwd=None, logger=None):
        self.__log = ""
        self.__env = env
        self.__cmdlines = []
        self.__logger = logger
        self.__cwd = cwd

    ##
    ## @brief      Adds a command that will be executed.
    ##             The command will be added with '&&' to the rest of the
    ##             commands
    ##
    ## @param      self     The object
    ## @param      cmdline  The cmdline
    ##
    ## @return     None
    ##
    def add_command(self, cmdline):
        if len(self.__cmdlines) != 0:
            self.__cmdlines.append('&&')

        if isinstance(cmdline, list):
            self.__cmdlines.extend(cmdline)
        else:
            self.__cmdlines.append(cmdline)


    ##
    ## @brief      clears all commands
    ##
    ## @param      self  The object
    ##
    ## @return     None
    ##
    def clear_commands(self):
        self.__cmdlines = []

    ##
    ## @brief      exectues all commands
    ##
    ## @param      self     The object
    ## @param      verbose  The verbose
    ##
    ## @return     None
    ##
    def execute(self, verbose=False):
        self.__logger.debug("Executing {0}".format(" ".join(self.__cmdlines)))
        proc = subprocess.Popen(self.__cmdlines,
                                cwd=self.__cwd,
                                env=self.__env,
                                stderr=subprocess.STDOUT,
                                stdout=subprocess.PIPE)
        stdout = ""
        for line in iter(proc.stdout.readline, b''):
            line_enc = line.decode(sys.stdout.encoding, 'ignore')
            stdout += line_enc + '\n'
            if verbose:
                sys.stdout.write(line_enc)

        proc.stdout.close()
        proc.wait()

        self.__log += stdout

        return proc.returncode, stdout


    ##
    ## \brief      exports the current shell into a python script
    ##
    ## \param      self      The object
    ## \param      filename  The filename
    ## \param      template  The template
    ## \param      config    The configuration
    ##
    ## \return     None
    ##
    def export_as_script(self, filename, template='run.tmpl', build_type='Debug'):
        script_dir = os.path.dirname(os.path.realpath(__file__))
        template_path = os.path.join(script_dir, '..', 'templates')

        cwd = self.__cwd.replace('\\', '/')

        with open(os.path.join(template_path, template)) as f:
            lines = f.readlines()
            test_script_content = ''.join(lines).format(IN_CMD=self.__cmdlines,
                                                        IN_CWD=cwd,
                                                        IN_BUILD_TYPE=build_type)

            with open(filename, 'w') as of:
                of.write(test_script_content)
            of.close()
        f.close()

    ##
    ## @brief      modifies the environment of the build shell
    ##
    ## @param      self    The object
    ## @param      key     The key
    ## @param      values  The values
    ## @param      sep     The separator 
    ## @param      append  append value to key if key exists
    ##
    ## @return     None
    ##
    def modify_env(self, key, values, sep=' ', append=False):
        if values is not None and len(values) > 0:
            self.__logger.debug("Modifying environment '{KEY}:{VALUE} [append={APPEND}]'".format(KEY=key,
                                                                                                 VALUE=values,
                                                                                                 APPEND=append))
            if isinstance(values, list):
                val = sep.join(values)
            else:
                val = values

            if append is True:
                if key in self.__env:
                    self.__env[key] += sep+val
                else:
                    self.__env[key] = val
            else:
                self.__env[key] = val


    ##
    ## @brief      modifies the path variable used by this build sehll
    ##
    ## @param      self    The object
    ## @param      paths   The paths
    ## @param      append  The append
    ##
    ## @return     None
    ##
    def modify_path(self, paths, append=True):
        if paths is not None:
            self.modify_env('PATH', paths, sep=os.pathsep, append=append)


    ##
    ## @brief      Loads a paths from file.
    ##
    ## @param      self      The object
    ## @param      filename  The filename
    ## @param      append    appends to the system path if set to True
    ##
    ## @return     return 0 if all went fine, 1 if not
    ##
    def load_path_from_file(self, filename, append=True):
        ret = 0
        if os.path.exists(filename):
            self.__logger.debug("Loading paths from '{0}'".format(filename))
            with open(filename, 'r') as f:
                content = f.readlines()
                content = [x.strip() for x in content]
                self.modify_path(content, append=append)
            f.close()
        else:
            self.__logger.debug("Could not load paths from '{0}'".format(filename))
            ret = 1

        return ret

    ##
    ## @brief      Loads an environment from file.
    ##
    ## @param      self      The object
    ## @param      filename  The filename
    ## @param      append    The append
    ##
    ## @return     return 0 if all went fine, 1 if not
    ##
    def load_env_from_file(self, filename, append=False):
        ret = 0
        if os.path.exists(filename):
            self.__logger.debug("Loading environment variables from '{0}'".format(filename))
            with open(filename, 'r') as f:
                content = f.readlines()

                for line in content:
                    m = re.match('([\w]+)=(.*)', line)
                    if m is not None:
                        self.modify_env(m.group(1).strip(), m.group(2).strip(), append=append)
            f.close()
        else:
            self.__logger.debug("Could not load paths from '{0}'".format(filename))
            ret = 1

        return ret

    ##
    ## @brief      Gets the log.
    ##
    ## @param      self  The object
    ##
    ## @return     The log.
    ##
    def get_log(self):
        return self.__log
