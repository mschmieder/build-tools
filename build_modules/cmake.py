# -*- coding: utf-8 -*-
#
#  Copyright (c) 2017 Matthias Schmieder
#  This file is part of build-tools (https://bitbucket.org/mschmieder/build-tools).
#
#  This file is licensed to you under your choice of the GNU Lesser
#  General Public License, version 3 or any later version (LGPLv3 or
#  later), or the GNU General Public License, version 2 (GPLv2), in all
#  cases as published by the Free Software Foundation.
#

import re
import shutil
import logging
from .build_utils import *


##
## @brief      Class for make.
##
class CMake(object):
    def __init__(self,
                 params=[],
                 logger = None):

        self.__cmake_exec_path = None
        self.__ctest_exec_path = None
        self.__version = None
        self.__params = params
        self.__logger = logger

        self.get_path()
        self.get_version()


    ##
    ## @brief      Gets the path.
    ##
    ## @param      self  The object
    ##
    ## @return     The path.
    ##
    def get_path(self):
        if self.__cmake_exec_path is None:
            self.__cmake_exec_path = shutil.which('cmake')
            if self.__cmake_exec_path is None:
                raise Exception("Error! CMake could not be found in the system path")

        return self.__cmake_exec_path

    ##
    ## @brief      Gets the ctest path.
    ##
    ## @param      self  The object
    ##
    ## @return     The ctest path.
    ##
    def get_ctest_path(self):
        if self.__ctest_exec_path is None:
            self.__ctest_exec_path = shutil.which('ctest')
            if self.__ctest_exec_path is None:
                raise Exception("Error! CMake/CTest could not be found in the system path")

        return self.__ctest_exec_path

    ##
    ## @brief      Gets the version.
    ##
    ## @param      self  The object
    ##
    ## @return     The version.
    ##
    def get_version(self):
        if self.__version is None:
            cmd = [self.get_path(),'--version']
            ret, stdout = build_utils.execute(cmd)
            if ret != 0:
                raise Exception("Error! Could not determine by calling '{0}'".format(' '.join(cmd)))

            for line in stdout:
                m = re.match(r'cmake version.*([0-9].[0-9].[0-9])', stdout)
                if m is not None:
                    self.__version = m.group(1).strip()
                    break

        return self.__version

    ##
    ## @brief      Gets the parameters.
    ##
    ## @param      self  The object
    ##
    ## @return     The parameters.
    ##
    def get_params(self):
        return self.__params
