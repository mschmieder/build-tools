# -*- coding: utf-8 -*-
#
#  Copyright (c) 2017 Matthias Schmieder
#  This file is part of build-tools (https://bitbucket.org/mschmieder/build-tools).
#
#  This file is licensed to you under your choice of the GNU Lesser
#  General Public License, version 3 or any later version (LGPLv3 or
#  later), or the GNU General Public License, version 2 (GPLv2), in all
#  cases as published by the Free Software Foundation.
#

import re
import logging 
import sys
import os
from .build_utils import *
from .clang import *

##
## @brief      Class for gcovr.
##
class Gcovr(object):
    def __init__(self, builder,
                       source_dir,
                       build_dir,
                       output_dir,
                       formatter = ['html', 'xml'],
                       logger = None):

        self.__builder = builder
        self.__source_dir = source_dir
        self.__build_dir = build_dir
        self.__formatter = formatter
        self.__output_dir = output_dir
        self.__logger = logger
    ##
    ## @brief      exectues gcovr
    ##
    ## @param      self  The object
    ##
    ## @return     return code and output
    ##
    def execute(self):
        os.makedirs(self.__output_dir, exist_ok=True)

        gcovr_cmd = [sys.executable,
                    self.get_path(),
                    '-s', '-r', self.__source_dir,
                    '--object-directory',self.__build_dir]

        gcov_cmd = self.__builder.get_gcov_command()
        if gcov_cmd is not None:
            gcovr_cmd.append('--gcov-executable')
            gcovr_cmd.append(' '.join(gcov_cmd))

        out_txt = ''
        for format_desc in self.__formatter:
            cmd = gcovr_cmd            
            if 'html' in format_desc:
                cmd.extend(['--html', '--html-details'])

            if 'xml' in format_desc:
                cmd.extend(['--html', '--html-details'])

            cmd.extend(['-o', os.path.join(self.__output_dir,'coverage.'+format_desc)])

            self.__logger.debug("Executing "+' '.join(cmd))
            ret, out = build_utils.execute(cmd, verbose=True) 

            if ret != 0:
                self.__logger.debug("Gcov command failed!")
                return 1, out

            out_txt += out

        return 0, out_txt


    ##
    ## @brief      Gets the path.
    ##
    ## @param      self  The object
    ##
    ## @return     The path.
    ##
    def get_path(self):
        candidates = ['gcovr',
                      os.path.realpath(os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", 'tools', 'gcovr'))]

        gcovr_exec = build_utils.find_executable(candidates)
        if gcovr_exec is None:
            self.__logger.critical("Could not locate scan-build! Check your installation")

        return gcovr_exec
