# -*- coding: utf-8 -*-
#
#  Copyright (c) 2017 Matthias Schmieder
#  This file is part of build-tools (https://bitbucket.org/mschmieder/build-tools).
#
#  This file is licensed to you under your choice of the GNU Lesser
#  General Public License, version 3 or any later version (LGPLv3 or
#  later), or the GNU General Public License, version 2 (GPLv2), in all
#  cases as published by the Free Software Foundation.
#

import re
import logging 
from .build_utils import *
from .clang import *

##
## @brief      Class for scan build.
##
class ScanBuild(object):
    def __init__(self, builder,
                       output_dir,
                       checkers = [],
                       logger = None):

        self.__builder = builder
        self.__checkers = checkers
        self.__output_dir = output_dir
        self.__logger = logger

        if self.__checkers is not None and 'all' in self.__checkers:
            self.__checkers = self.get_available_checkers()

    ##
    ## @brief      Gets the name.
    ##
    ## @param      self  The object
    ##
    ## @return     The name.
    ##
    def get_name(self):
        return 'scan-build'

    ##
    ## @brief      Gets the command.
    ##
    ## @param      self  The object
    ##
    ## @return     The command.
    ##
    def get_pre_build_commands(self):
        scan_cmd = [self.get_path(),
                    '-v', '-k',
                    '-o', self.__output_dir,
                    '--use-cc', self.__builder.get_cc_compiler(),
                    '--use-c++', self.__builder.get_cxx_compiler()]

        for checker in self.get_available_checkers():
            scan_cmd.extend(['-enable-checker', checker])

        return scan_cmd

    ##
    ## \brief      Gets the pre configuration commands.
    ##
    ## \param      self  The object
    ##
    ## \return     The pre configuration commands.
    ##
    def get_pre_configuration_commands(self):
        return self.get_pre_build_commands()

    ##
    ## @brief      Gets the available checkers.
    ##
    ## @param      self  The object
    ##
    ## @return     The available checkers.
    ##
    def get_available_checkers(self):
        if self.__checkers is None:
            self.__checkers = []
            if isinstance(self.__builder, Clang) is False:
                return []

            cmd = [ self.__builder.get_cxx_compiler(),
                    '-cc1', '-analyzer-checker-help' ]

            ret, stdout = build_utils.execute(cmd)

            if ret != 0:
                raise "Error! Command '{0}' failed".format(' '.join(cmd))

            for line in stdout.split('\n'):
                m = re.match(r'^\s+((\w+\.)+(\w+))', line)
                if m is not None:
                    checker_name = m.group(0).strip()
                    if "debug" not in checker_name:
                        self.__checkers.append(checker_name)

        return self.__checkers

    ##
    ## @brief      Gets the path.
    ##
    ## @param      self  The object
    ##
    ## @return     The path.
    ##
    def get_path(self):
        candidates = ["scan-build-{0}".format(self.__builder.get_version()),
                      "scan-build-mp-{0}".format(self.__builder.get_version()),
                      "scan-build"]

        scan_build_exec = build_utils.find_executable(candidates)
        if scan_build_exec is None:
            self.__logger.critical("Could not locate scan-build! Check your installation")

        return scan_build_exec
