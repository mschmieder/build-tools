# -*- coding: utf-8 -*-
#
#  Copyright (c) 2017 Matthias Schmieder
#  This file is part of build-tools (https://bitbucket.org/mschmieder/build-tools).
#
#  This file is licensed to you under your choice of the GNU Lesser
#  General Public License, version 3 or any later version (LGPLv3 or
#  later), or the GNU General Public License, version 2 (GPLv2), in all
#  cases as published by the Free Software Foundation.
#

import re
import logging 
import os
from .build_utils import *

if sys.platform.startswith("win"):
    import winreg


##
## @brief      Class for cpp check.
##
class CppCheck(object):
    def __init__(self, logger = None):
        self.__logger = logger
        self.__path = None

    def get_version(self):
        version = None
        ret, out = build_utils.execute([self.get_cppcheck_path(), '--version'])
        if ret is 0:
            m = re.search(r'([0-9.]+)', out)
            if m is not None:
                version = m.group(0)

        return version

    ##
    ## @brief      executes cppcheck
    ##
    ## @param      self           The object
    ## @param      source_dir     The source dir
    ## @param      output_dir     The output dir
    ## @param      output_type    The output type
    ## @param      verbose        enables verbose output
    ## @param      include_files  The include files
    ## @param      num_threads    The number threads
    ## @param      checkers       The checkers
    ##
    ## @return     return code and output
    ##
    def execute(self, source_dir, output_dir, output_type='xml', verbose=False, include_files = None, num_threads = os.cpu_count(), checkers=['warning', 'performance', 'information', 'style']):
        os.makedirs(output_dir, exist_ok=True)
        
        cmd = [self.get_cppcheck_path(),
               '-j', str(num_threads),
               '--enable={0}'.format(','.join(checkers)),
               '--xml-version=2',
               source_dir]

        if include_files is not None:
          cmd.extend(['--includes-file',' '.join(include_files)])

        self.__logger.debug("Execute {0}".format(' '.join(cmd)))

        # pipe stderr to outout file since this holds the actual data
        f = open(os.path.join(output_dir,'cppcheck.xml'), 'w')
        process = subprocess.Popen(cmd, stderr=f, stdout=subprocess.PIPE)

        out = ""
        for line in iter(process.stdout.readline, b''):
            out += line.decode(sys.stdout.encoding, 'ignore') + '\n'
            if verbose:
                sys.stdout.write(line.decode(sys.stdout.encoding, 'ignore'))
        process.wait()
        f.close()

        if process.returncode != 0:
            self.__logger.critical("Call to cppcheck failed")


        if 'html' in output_type:
            ret, out = self.create_htmlreport(source_dir=source_dir,
                                              input_file=os.path.join(output_dir,'cppcheck.xml'),
                                              output_dir=os.path.join(output_dir,'cppcheck'))
            if ret != 0:
                self.__logger.critical("Call to cppcheck-htmlreport failed")

        return process.returncode, out


    ##
    ## @brief      Creates a htmlreport. (only works with python2)
    ##
    ## @param      self        The object
    ## @param      source_dir  The source dir
    ## @param      input_file  The input file
    ## @param      output_dir  The output dir
    ##
    ## @return     return code and output
    ##
    def create_htmlreport(self, source_dir, input_file, output_dir):
        cmd = [self.get_cppcheck_htmlreport_path(),
               '--source-dir',source_dir,
               '--report-dir', output_dir,
               '--file',input_file]

        return build_utils.execute(cmd, verbose=True)


    ##
    ## @brief      Gets the cppcheck path.
    ##
    ## @param      self  The object
    ##
    ## @return     The cppcheck path.
    ##
    def get_cppcheck_path(self):
        if self.__path is None:
            candidates = ["cppcheck"]
            exec_path = build_utils.find_executable(candidates)

            if exec_path is None:
                self.__logger.debug("Cppecheck not found in system path...")
                if 'windows' in build_utils.get_host_system():
                    self.__logger.debug("Searching for Cppcheck in windows registry...")
                    try:
                        reg = winreg.OpenKey(winreg.HKEY_CURRENT_USER, r'Software\Cppcheck', 0, winreg.KEY_READ)
                        exec_path = list(winreg.QueryValueEx(reg, 'InstallationPath'))[0]
                        if exec_path is not None:
                            exec_path = os.path.join(exec_path, 'cppcheck')
                    except:
                        self.__logger.warning("Cppecheck could not be found in windows registry")

            if exec_path is None:
                self.__logger.critical("Cppecheck was not found on system")
            else:
                self.__path = os.path.normpath(exec_path)
                self.__logger.debug("Cppecheck was found at location '{0}'".format(exec_path))

        return self.__path


    ##
    ## @brief      Gets the cppcheck htmlreport path.
    ##
    ## @param      self  The object
    ##
    ## @return     The cppcheck htmlreport path.
    ##
    def get_cppcheck_htmlreport_path(self):
        candidates = ["cppcheck-htmlreport"]
        exec_path = build_utils.find_executable(candidates)

        if exec_path is None:
          self.__logger.critical("Was not able to locate cppcheck. Please check your installation!")

        return exec_path
