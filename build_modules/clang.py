# -*- coding: utf-8 -*-
#
#  Copyright (c) 2017 Matthias Schmieder
#  This file is part of build-tools (https://bitbucket.org/mschmieder/build-tools).
#
#  This file is licensed to you under your choice of the GNU Lesser
#  General Public License, version 3 or any later version (LGPLv3 or
#  later), or the GNU General Public License, version 2 (GPLv2), in all
#  cases as published by the Free Software Foundation.
#

import shutil
import re
import logging
import os
from .build_utils import *

if sys.platform.startswith("win"):
    import winreg

##
## @brief      Class for clang.
##
class Clang(object):
    def __init__(self, compiler_id,
                       arch,
                       coverage = False,
                       cmake_generator = None,
                       clang_toolchain_name = None,
                       logger = None):
    
        self.__compiler_id=compiler_id
        self.__arch=arch
        self.__coverage = coverage
        self.__cmake_generator = cmake_generator
        self.__clang_toolchain_name = clang_toolchain_name
        self.__logger = logger


    ##
    ## @brief      Gets the version.
    ##
    ## @param      self  The object
    ##
    ## @return     The version.
    ##
    def get_version(self):
        return re.sub(r'[a-zA-Z-]+', '', self.__compiler_id)


    ##
    ## @brief      Gets the cxx compiler.
    ##
    ## @param      self  The object
    ##
    ## @return     The cxx compiler.
    ##
    def get_cxx_compiler(self):
        candidates = ["clang++-{0}".format(self.get_version()),
                      "clang++-mp-{0}".format(self.get_version()),
                      "clang++{0}".format(self.get_version()),
                      "clang++" # on OSX llvm installed with brew does not provide
                                # symlinks to clang++-VERSION only to clang-VERSION
                                # so we assume that the 'right' clang++ comes first 
                                # within the environment path
                      ]

        compiler_path = build_utils.find_executable(candidates)

        # check if host system is windows
        if compiler_path is None:
            if sys.platform.startswith("win"):
                compiler_path = self.__find_clang_cl()

        if compiler_path is None:
            raise Exception("Error! Could not find clang cxx compiler")

        return compiler_path


    ##
    ## @brief      Gets the gcov command.
    ##
    ## @param      self  The object
    ##
    ## @return     The gcov command.
    ##
    def get_gcov_command(self):
        candidates = ["llvm-cov-{0}".format(self.get_version()),
                      "llvm-cov-mp-{0}".format(self.get_version()),
                      "llvm-cov{0}".format(self.get_version()),
                      "llvm-cov" # on OSX llvm installed with brew does not provide
                                 # symlinks to llvm-cov-VERSION
                     ]


        gcov_path = build_utils.find_executable(candidates)

        if gcov_path is None:
            raise Exception("Error! Could not find gcov tool")

        return [gcov_path,'gcov']

    ##
    ## @brief      Gets the cc compiler.
    ##
    ## @param      self  The object
    ##
    ## @return     The cc compiler.
    ##
    def get_cc_compiler(self):
        candidates = ["clang-{0}".format(self.get_version()),
                      "clang-mp-{0}".format(self.get_version()),
                      "clang{0}".format(self.get_version())]

        compiler_path = build_utils.find_executable(candidates)

        # check if host system is windows
        if compiler_path is None:
            if sys.platform.startswith("win"):
                compiler_path = self.__find_clang_cl()

        if compiler_path is None:
            raise Exception("Error! Could not find clang cc compiler")

        return compiler_path    


    ##
    ## @brief      Gets the default cmake generator.
    ##
    ## @param      self  The object
    ##
    ## @return     The default cmake generator.
    ##
    def get_default_cmake_generator(self):
        if self.__cmake_generator is None:
            if sys.platform.startswith("win"):
                lst_versions = self.__get_available_msvc_versions()
                latest_version = lst_versions[-1]
                self.__cmake_generator = "Visual Studio {0}".format(latest_version)
                if self.__arch == "x86_64":
                    return self.__cmake_generator + " Win64"
            else:
                self.__cmake_generator = "Unix Makefiles"

        return self.__cmake_generator


    ##
    ## @brief      Gets the additional cmake options.
    ##
    ## @param      self  The object
    ##
    ## @return     The additional cmake options.
    ##
    def get_additional_cmake_options(self):
        lst_options = []
        if sys.platform.startswith("win"):
            if self.__clang_toolchain_name is None:
                # currently there is no other name
                lst_versions = self.__get_available_msvc_versions()
                latest_version = lst_versions[-1]
                # 11 --> 2012, 12 --> 2013, 14 --> 2015, 15 --> 2017 (!)
                if int(latest_version) < 15:
                    lst_options.append('-TLLVM-vs20{0}'.format(latest_version))
                else:
                    # currently there is no specific toolchain for VS 2017!
                    lst_options.append('-TLLVM-vs2014')
            else:
                lst_options.append('-T{0}'.format(self.__clang_toolchain_name))

        return lst_options


    ##
    ## @brief      Gets the cxx flags.
    ##
    ## @param      self  The object
    ##
    ## @return     The cxx flags.
    ##
    def get_cxx_flags(self):
        cxx_flags = ""
        if self.__arch == "x86_64":
            cxx_flags += ' -m64'
        elif self.__arch == "x86":
            cxx_flags += ' -m32'

        if self.__coverage:
            cxx_flags += ' --coverage'

        return cxx_flags.strip()

    ##
    ## @brief      Gets the c flags.
    ##
    ## @param      self  The object
    ##
    ## @return     The c flags.
    ##
    def get_c_flags(self):
        return self.get_cxx_flags()


    ##
    ## @brief      Gets the ld flags.
    ##
    ## @param      self  The object
    ##
    ## @return     The ld flags.
    ##
    def get_ld_flags(self):
        return ""


    ##
    ## @brief      Gets the pre configuration commands.
    ##
    ## @param      self  The object
    ##
    ## @return     The pre configuration commands.
    ##
    def get_pre_configuration_commands(self):
        return []


    ##
    ## @brief      looks for clang installed on a windows machine
    ##
    ## @param      self  The object
    ##
    ## @return     path to clang
    ##
    def __find_clang_cl(self):
        compiler_path = None
        key_dir = r'SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\LLVM'
        try:
            reg = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, key_dir, 0, winreg.KEY_WOW64_64KEY + winreg.KEY_READ)
            llvm_dir = list(winreg.QueryValueEx(reg, 'DisplayIcon'))[0]
            llvm_version = list(winreg.QueryValueEx(reg, 'DisplayVersion'))[0]
            compiler_path = os.path.join(llvm_dir, 'bin', 'clang-cl')
            if self.get_version() != "" and self.get_version() not in llvm_version:
                self.__logger.critical("Found clang at {0} but versions required version missmatches {1} vs. {2}"
                                       .format(llvm_dir, self.get_version(), llvm_version))
        except:
            self.__logger.critical("Clang could not be found within windows registry.")

        return compiler_path


    ##
    ## @brief      Gets the available msvc versions.
    ##
    ## @param      self  The object
    ##
    ## @return     The available msvc versions.
    ##
    def __get_available_msvc_versions(self):
        key_dir = r'SOFTWARE\Wow6432Node\Microsoft\VisualStudio\SxS\VS7'
        reg = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, key_dir, 0, winreg.KEY_WOW64_64KEY + winreg.KEY_READ)

        lst_available_versions = []
        for i in range(0, winreg.QueryInfoKey(reg)[1]):
            lst_available_versions.append((winreg.EnumValue(reg, i))[0])

        for idx, value in enumerate(lst_available_versions):
            lst_available_versions[idx] = value.replace('.0', '')

        lst_available_versions.sort()
        return lst_available_versions
