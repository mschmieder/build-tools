# -*- coding: utf-8 -*-
#
#  Copyright (c) 2017 Matthias Schmieder
#  This file is part of build-tools (https://bitbucket.org/mschmieder/build-tools).
#
#  This file is licensed to you under your choice of the GNU Lesser
#  General Public License, version 3 or any later version (LGPLv3 or
#  later), or the GNU General Public License, version 2 (GPLv2), in all
#  cases as published by the Free Software Foundation.
#


import re
import logging 
from .build_utils import *
from .msvc import Msvc
from .gcc import Gcc

class Coverity(object):
    def __init__(self, builder,
                       build_dir,
                       output_dir,
                       checkers=[],
                       aggressiveness_level='medium',
                       logger=None):

        self.__build_dir = build_dir
        self.__cov_config_dir = os.path.join(self.__build_dir, 'coverity')
        self.__cov_config_file = os.path.join(self.__cov_config_dir,'coverity_config.xml')
        self.__builder = builder
        self.__output_dir = output_dir
        self.__checkers = checkers
        self.__logger = logger
        self.__aggressiveness_level=aggressiveness_level

    def get_name(self):
        return 'coverity'


    def get_pre_build_commands(self):
        cov_build = [self.get_cov_build_path(),
                      "--config", self.__cov_config_file,
                      "--dir", self.__output_dir]

        return cov_build
    
    def get_available_checkers(self):
        return []

    def get_cov_configure_path(self):
        candidates = ["cov-configure"]

        cov_configure = build_utils.find_executable(candidates)
        if cov_configure is None:
            self.__logger.critical("Could not locate Coverity! Check your installation")

        return cov_configure

    def get_cov_build_path(self):
        candidates = ["cov-build"]

        cov_build = build_utils.find_executable(candidates)
        if cov_build is None:
            self.__logger.critical("Could not locate Coverity! Check your installation")

        return cov_build


    def get_cov_analyze_path(self):
        candidates = ["cov-analyze"]

        cov_analyze = build_utils.find_executable(candidates)
        if cov_analyze is None:
            self.__logger.critical("Could not locate Coverity! Check your installation")

        return cov_analyze


    def analyze(self):
        analyze_command = [self.get_cov_analyze_path(),
                           '--dir', self.__output_dir,
                           '--aggressiveness-level', self.__aggressiveness_level]

        checkers = self.__checkers
        checkers.append('--concurrency')
        checkers.append('--enable-virtual')
        checkers.append('--enable-constraint-fpp')
        checkers.append('--enable-fnptr')
        checkers.append('--enable-callgraph-metrics')

        analyze_command.extend(checkers)

        return build_utils.execute(analyze_command, verbose=True)


    def get_pre_configuration_commands(self):
        os.makedirs(self.__cov_config_dir, exist_ok=True)
        cov_configure = [self.get_cov_configure_path(),
                         "--config", self.__cov_config_file,
                         "--compiler", os.path.basename(self.__builder.get_cc_compiler()),
                         '--template', '&&']

        return cov_configure


    def __get_coverity_compiler_id(self):
        if isinstance(self.__builder, Msvc):
            return 'msvc'
        elif isinstance(self.__builder, Gcc):
            return 'gcc'
        else:
            raise Exception("Unsupported compiler!")