# -*- coding: utf-8 -*-
#
#  Copyright (c) 2017 Matthias Schmieder
#  This file is part of build-tools (https://bitbucket.org/mschmieder/build-tools).
#
#  This file is licensed to you under your choice of the GNU Lesser
#  General Public License, version 3 or any later version (LGPLv3 or
#  later), or the GNU General Public License, version 2 (GPLv2), in all
#  cases as published by the Free Software Foundation.
#

import re
import logging 
import glob
import fnmatch
import os
from .build_utils import *

if sys.platform.startswith("win"):
    import winreg

##
## @brief      Class for cccc.
##
class CCCC(object):
    def __init__(self,
                 scan_dir,
                 output_dir,
                 scan_file_patterns=['*.h', '*.hh', '*.hpp', '*.c', '*.cc', '*.cpp'],
                 exclude_pattern=[],
                 logger = None):

        self.__scan_file_patterns = scan_file_patterns
        self.__scan_dir = scan_dir
        self.__output_dir = output_dir
        self.__exclude_pattern = exclude_pattern
        self.__logger = logger

    def get_version(self):
        version = None
        ret, out = build_utils.execute([self.get_cccc_path()])
        if ret is 0:
            m = re.search(r'Version ([0-9.]+)', out)
            if m is not None:
                version = m.group(0)

        return version

    ##
    ## @brief      executes cccc
    ##
    ## @param      self The object
    ## @return     return code and output
    ##
    def execute(self, verbose=True):
        _tmp_files = []
        for pattern in self.__scan_file_patterns:
            for root, dirnames, filenames in os.walk(self.__scan_dir):
                for filename in fnmatch.filter(filenames, pattern):
                    _tmp_files.append(os.path.join(root, filename))
                 
        # exlude files
        files_to_check = []

        if len(self.__exclude_pattern) == 0:
            files_to_check = _tmp_files

        for pattern in self.__exclude_pattern:
            for file in files_to_check:
                m = re.match(pattern, file)
                if m is None:
                    files_to_check.append(file)
                    self.__logger.debug("Scanning file '{0}'".format(file))
                else:
                    self.__logger.debug("Excluding file '{0}'".format(file))

        os.makedirs(self.__output_dir, exist_ok=True)
        
        cccc_cmd = [self.get_cccc_path(),
                    '--outdir={0}'.format(self.__output_dir)]
        cccc_cmd.extend(files_to_check)

        return build_utils.execute(cccc_cmd, verbose=verbose)


    ##
    ## @brief      Gets the cccc path.
    ##
    ## @param      self  The object
    ##
    ## @return     The cccc path.
    ##
    def get_cccc_path(self):
        candidates = ["cccc"]
        exec_path = build_utils.find_executable(candidates)

        if exec_path is None:
          self.__logger.critical("Was not able to locate cccc. Please check your installation!")

        return exec_path
  