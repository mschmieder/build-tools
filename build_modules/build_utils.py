# -*- coding: utf-8 -*-
#
#  Copyright (c) 2017 Matthias Schmieder
#  This file is part of build-tools (https://bitbucket.org/mschmieder/build-tools).
#
#  This file is licensed to you under your choice of the GNU Lesser
#  General Public License, version 3 or any later version (LGPLv3 or
#  later), or the GNU General Public License, version 2 (GPLv2), in all
#  cases as published by the Free Software Foundation.
#

import subprocess
import sys
import platform
import shutil
import os
from zipfile import ZipFile, ZipInfo

##
## @brief      Wrapper for ZipFile that has an issue preserving the file flags
##
class ZipFileEx(ZipFile):

    def extract(self, member, path=None, pwd=None):
        if not isinstance(member, ZipInfo):
            member = self.getinfo(member)

        if path is None:
            path = os.getcwd()

        ret_val = self._extract_member(member, path, pwd)
        attr = member.external_attr >> 16
        os.chmod(ret_val, attr)
        return ret_val


##
## @brief      Class for build utilities.
##
class build_utils(object):

    ##
    ## @brief      Gets the host system.
    ##
    ## @return     The host system.
    ##
    @staticmethod
    def get_host_system():
        platform_system = platform.system()
        if platform_system == "Windows" : return "windows"
        if platform_system == "Linux" : return "linux"
        if platform_system == "Darwin" : return "macos"


    ##
    ## @brief      Appends to dictionary.
    ##
    ## @param      val_dict  The value dictionary
    ## @param      id        The identifier
    ## @param      value     The value
    ## @param      sep       The separator
    ##
    ## @return     the dictionary
    ##
    @staticmethod
    def append_to_dict(val_dict, id, value, sep = ' '):
        if value is not None:
            if id in val_dict:
                if isinstance(value, list):
                    val_dict[id] += sep.join(value)
                else:
                    val_dict[id] += sep+value
            else:
                val_dict[id] = value

        return val_dict

    ##
    ## @brief      executes a given command
    ##
    ## @param      cmd      The command
    ## @param      env      The environment
    ## @param      verbose  The verbose
    ##
    ## @return     return value and output
    ##
    @staticmethod
    def execute(cmd, env=None, cwd=None, verbose = False):
        process = subprocess.Popen(cmd, env=env, cwd=cwd, stderr=subprocess.STDOUT, stdout=subprocess.PIPE)

        stdout = ""
        for line in iter(process.stdout.readline, b''):
            stdout += line.decode(sys.stdout.encoding, 'ignore') + "\n"
            if verbose:
                sys.stdout.write(line.decode(sys.stdout.encoding, 'ignore'))
        process.wait()

        return process.returncode, stdout

    ##
    ## @brief      looks for an exectuable in your system path
    ##
    ## @param      list_names  The list names
    ##
    ## @return     the path to the executable
    ##
    @staticmethod
    def find_executable(list_names):
        for name in list_names:
            if shutil.which(name) is not None:
                return shutil.which(name)

        return None


    