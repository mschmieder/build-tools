# -*- coding: utf-8 -*-
#
#  Copyright (c) 2017 Matthias Schmieder
#  This file is part of build-tools (https://bitbucket.org/mschmieder/build-tools).
#
#  This file is licensed to you under your choice of the GNU Lesser
#  General Public License, version 3 or any later version (LGPLv3 or
#  later), or the GNU General Public License, version 2 (GPLv2), in all
#  cases as published by the Free Software Foundation.
#

import shutil
import re
import logging
from .build_utils import *


##
## @brief      Class for gcc.
##
class Gcc(object):
    def __init__(self, compiler_id,
                       arch,
                       coverage = False,
                       cmake_generator = None,
                       logger = None):
    
        self.__compiler_id=compiler_id
        self.__arch=arch
        self.__coverage = coverage
        self.__cmake_generator = cmake_generator
        self.__logger = logger


    ##
    ## @brief      Gets the version.
    ##
    ## @param      self  The object
    ##
    ## @return     The version.
    ##
    def get_version(self):
        return re.sub(r'[a-zA-Z-]+', '', self.__compiler_id)


    ##
    ## @brief      Gets the cxx compiler.
    ##
    ## @param      self  The object
    ##
    ## @return     The cxx compiler.
    ##
    def get_cxx_compiler(self):
        candidates = ["g++-{0}".format(self.get_version()),
                      "g++-mp-{0}".format(self.get_version()),
                      "g++{0}".format(self.get_version())]


        compiler_path = build_utils.find_executable(candidates)

        if compiler_path is None:
            raise Exception("Error! Could not find gcc cxx compiler")

        return compiler_path

    ##
    ## @brief      Gets the cc compiler.
    ##
    ## @param      self  The object
    ##
    ## @return     The cc compiler.
    ##
    def get_cc_compiler(self):
        candidates = ["gcc-{0}".format(self.get_version()),
                      "gcc-mp-{0}".format(self.get_version()),
                      "gcc{0}".format(self.get_version())]

        compiler_path = build_utils.find_executable(candidates)

        if compiler_path is None:
            raise Exception("Error! Could not find gcc cc compiler")

        return compiler_path    

    ##
    ## @brief      Gets the gcov command.
    ##
    ## @param      self  The object
    ##
    ## @return     The gcov command.
    ##
    def get_gcov_command(self):
        candidates = ["gcov-{0}".format(self.get_version()),
                      "gcov-mp-{0}".format(self.get_version()),
                      "gcov{0}".format(self.get_version())]


        gcov_path = build_utils.find_executable(candidates)

        if gcov_path is None:
            raise Exception("Error! Could not find clang cxx compiler")

        return [gcov_path]

    ##
    ## @brief      Gets the default cmake generator.
    ##
    ## @param      self  The object
    ##
    ## @return     The default cmake generator.
    ##
    def get_default_cmake_generator(self):
        if self.__cmake_generator is None:
            self.__cmake_generator = "Unix Makefiles"

        return self.__cmake_generator


    ##
    ## @brief      Gets the additional cmake options.
    ##
    ## @param      self  The object
    ##
    ## @return     The additional cmake options.
    ##
    def get_additional_cmake_options(self):
        lst_options = []
        return lst_options


    ##
    ## @brief      Gets the cxx flags.
    ##
    ## @param      self  The object
    ##
    ## @return     The cxx flags.
    ##
    def get_cxx_flags(self):
        flags = ""
        if self.__arch == "x86_64":
            flags += ' -m64'
        elif self.__arch == "x86":
            flags += ' -m32'

        if self.__coverage:
            flags += ' --coverage'

        return flags.strip()

    ##
    ## @brief      Gets the c flags.
    ##
    ## @param      self  The object
    ##
    ## @return     The c flags.
    ##
    def get_c_flags(self):
        return self.get_cxx_flags()

    ##
    ## @brief      Gets the ld flags.
    ##
    ## @param      self  The object
    ##
    ## @return     The ld flags.
    ##
    def get_ld_flags(self):
        return ""

    ##
    ## @brief      Gets the pre configuration commands.
    ##
    ## @param      self  The object
    ##
    ## @return     The pre configuration commands.
    ##
    def get_pre_configuration_commands(self):
        return []