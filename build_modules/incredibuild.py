# -*- coding: utf-8 -*-
#
#  Copyright (c) 2017 Matthias Schmieder
#  This file is part of build-tools (https://bitbucket.org/mschmieder/build-tools).
#
#  This file is licensed to you under your choice of the GNU Lesser
#  General Public License, version 3 or any later version (LGPLv3 or
#  later), or the GNU General Public License, version 2 (GPLv2), in all
#  cases as published by the Free Software Foundation.


import re
import logging
import glob
import fnmatch
import os
from .build_utils import *

if sys.platform.startswith("win"):
    import winreg


##
## @brief      Class for cccc.
##
class IncrediBuild(object):
    def __init__(self, logger=None):
        self.__logger = logger

    ##
    ## @brief      checks if incredbuild is available on the system
    ##
    ## @param      self  The object
    ##
    ## @return     The BuildConsole path.
    ##
    def is_available(self):
        if self.get_build_console_path(silent=True) is None:
            return False
        else:
            return True


    def version(self):
        ret, out =  build_utils.execute([self.get_build_console_path(), '/HELP'])
        if ret is 0:
            m = re.search(r'Build Acceleration Console ([0-9.]+\s.*\))', out)
            if m is not None:
                version = m.group(1)
        return version

    ##
    ## \brief      Returns the build a command.
    ##
    ## \param      self                 The object
    ## \param      config               The configuration
    ## \param      target_architecture  The target architecture
    ## \param      build_directory      The build directory
    ## \param      compiler             The compiler
    ## \param      target               The target
    ## \param      solution             The solution
    ## \param      rebuild              The rebuild
    ##
    ## \return     The command.
    ##
    def build_command(self,
                      config,
                      target_architecture,
                      build_directory=os.getcwd(),
                      compiler="",
                      target=None,
                      solution=None,
                      rebuild=False):

        cmd = [self.get_build_console_path()]

        cfg_arch = ""
        if "msvc" in compiler:
            if target_architecture == "x86_64":
                cfg_arch = '|x64'
            elif target_architecture == "x86":
                cfg_arch = "|x86"
            else:
                self.__logger.critical('Error: Unsupported architecture {}'.format(target_architecture))

        if solution is not None:
            cmd.append([solution])
        else:
            if "msvc" in compiler:
                # find solution file in build directory
                sln = glob.glob(os.path.join(build_directory, "*.sln"))
                cmd.append(sln[0])

        if target is not None:
            cmd.append(['/prj={}'.format(target)])

        cmd.extend(['/rebuild' if rebuild else '/build',
                    '/cfg={0}{1}'.format(config.title(), cfg_arch)])

        return cmd


    ##
    ## @brief      Gets the build console path.
    ##
    ## @param      self  The object
    ##
    ## @return     The BuildConsole path.
    ##
    def get_build_console_path(self, silent=True):
        candidates = ["BuildConsole"]
        exec_path = build_utils.find_executable(candidates)

        if exec_path is None and silent is False:
            self.__logger.critical("Was not able to locate IncrediBuild BuildConsole. Please check your installation!")

        return exec_path
