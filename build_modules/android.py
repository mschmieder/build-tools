# -*- coding: utf-8 -*-
#
#  Copyright (c) 2017 Matthias Schmieder
#  This file is part of build-tools (https://bitbucket.org/mschmieder/build-tools).
#
#  This file is licensed to you under your choice of the GNU Lesser
#  General Public License, version 3 or any later version (LGPLv3 or
#  later), or the GNU General Public License, version 2 (GPLv2), in all
#  cases as published by the Free Software Foundation.
#

import re
import os
import logging 
import zipfile
import urllib.request
from .build_utils import *
from .cmake import CMake
import stat

try:
    import pexpect                   
except:
    print("ATTENTION: Please install pexpect package if you want to use AndroidSDK")

##
## @brief      Class for android sdk.
##
class AndroidSDK(object):
    def __init__(self,
                 compiler_id,
                 sdk_root,
                 target_architecture='armeabi-v7a',
                 android_stl='gnustl_shared',
                 native_api_level='23',
                 ndk_platform='android-25',
                 build_tools_version='25.0.3',
                 tools_version='25.0.2',
                 logger=None):

        self.__compiler_id = compiler_id
        self.__android_stl = android_stl
        self.__sdk_root = sdk_root
        self.__target_architecture = target_architecture
        self.__native_api_level = native_api_level
        self.__tools_version = tools_version
        self.__ndk_platform = ndk_platform
        self.__build_tools_version = tools_version
        self.__logger = logger
        self.__cmake_root = None

    ##
    ## @brief      installs android sdk and ndk using the sdktools application
    ##
    ## @param      self     The object
    ## @param      verbose  enables verbose output
    ##
    ## @return     None
    ##
    def install(self, verbose=True):
        self.__logger.debug("Setting up AndroidSDK...")
        # create sdk root if does not exist
        os.makedirs(self.__sdk_root, exist_ok=True)

        # get sdk manager path
        sdkmanager = os.path.join(self.__sdk_root, 'tools', 'bin', 'sdkmanager')

        # check if sdkmanager is already available
        if os.path.exists(sdkmanager) is False:
            self.download()

        install_cmd = [sdkmanager,
                       'build-tools;{0}'.format(self.__build_tools_version),
                       'platforms;{0}'.format(self.__ndk_platform),
                       'ndk-bundle']

        child = pexpect.spawn(' '.join(install_cmd))
        if verbose:
            child.logfile = sys.stdout.buffer

        # wait until shell asks for 'Accept'
        while True:
            idx = child.expect(['Accept?', 'done', pexpect.EOF, pexpect.TIMEOUT], timeout=600)
            if idx == 0:
                child.sendline('y')
            if idx == 1 or idx == 2:
                break
            if idx == 3:
                self.__logger.critical('Timeout for installing package')
                break

        self.__install_cmake()


    ##
    ## @brief      install cmake as part of the SDK
    ##
    ## @param      self     The object
    ## @param      verbose  enables verbose output
    ##
    ## @return     None
    ##
    def __install_cmake(self, verbose=False):
        sdkmanager = os.path.join(self.__sdk_root, 'tools', 'bin', 'sdkmanager')
        ret, stdout = build_utils.execute([sdkmanager, '--list'])

        package_id = None
        package_version = None
        for line in stdout.split('\n'):
            m = re.match(r'\s+(cmake;([0-9.]+))', line)
            if m is not None:
                package_id = m.group(1)
                package_version = m.group(2)
                break

        if package_id is not None:
            self.__logger.debug("Installing package '{0}' ...".format(package_id))
            child = pexpect.spawn(' '.join([sdkmanager, package_id]))
            if verbose:
                child.logfile = sys.stdout.buffer

            # wait until shell asks for 'Accept'
            while True:
                idx = child.expect(['Accept?', 'done', pexpect.EOF, pexpect.TIMEOUT], timeout=600)
                if idx == 0:
                    child.sendline('y')
                if idx == 1 or idx == 2:
                    break
                if idx == 3:
                    self.__logger.critical('Timeout for installing package')
                    break

        # setup cmake system
        self.__cmake_root = os.path.join(self.__sdk_root, 'cmake', package_version)
        cmake_bin_dir = os.path.join(self.__cmake_root, 'bin')

        self.__logger.debug("Prepending cmake binary dir to path '{0}'".format(cmake_bin_dir))
        os.environ["PATH"] = cmake_bin_dir + os.pathsep + os.environ["PATH"]


    ##
    ## @brief      download the sdk from a given location
    ##
    ## @param      self     The object
    ## @param      dl_root  The download root address where to find the sdktools
    ##
    ## @return     None
    ##
    def download(self, dl_root = 'https://dl.google.com/android/repository/'):
        if 'win' in build_utils.get_host_system():
            dl_filename = 'tools_r{tools_version}-windows.zip'.format(tools_version=self.__tools_version)
        elif 'linux' in build_utils.get_host_system():
            dl_filename = 'tools_r{tools_version}-linux.zip'.format(tools_version=self.__tools_version)
        elif 'macos' in build_utils.get_host_system():
            dl_filename = 'tools_r{tools_version}-macosx.zip'.format(tools_version=self.__tools_version)


        temp_file = os.path.join(self.__sdk_root, dl_filename)
        if os.path.exists(temp_file) is False:
            url = dl_root+dl_filename
            self.__logger.debug("Downloading sdk tools from {0} ...".format(url))
            urllib.request.urlretrieve(url, temp_file)
        
        self.__logger.debug("Extracting sdk tools to sdk root {0} ...".format(self.__sdk_root))   
        zfile = ZipFileEx(temp_file)
        zfile.extractall(self.__sdk_root)
        zfile.close()
        os.remove(temp_file)


    ##
    ## @brief      updates all installed tools and platforms to the latest version
    ##
    ## @param      self     The object
    ## @param      verbose  enables verbose output
    ##
    ## @return     { description_of_the_return_value }
    ##
    def update(self, verbose = True):
        sdkmanager = os.path.join(self.__sdk_root, 'tools', 'bin', 'sdkmanager')
        sdkmanager_cmd = [sdkmanager, '--update']

        child = pexpect.spawn(' '.join(sdkmanager_cmd))
        if verbose:
            child.logfile = sys.stdout.buffer
        self.__logger("updating...")
        # wait until shell asks for 'Accept'
        while True:
            idx = child.expect(['Accept?', 'done', pexpect.EOF, pexpect.TIMEOUT], timeout=600)
            if idx == 0:
                child.sendline('y')
            if idx == 1 or idx == 2:
                break
            if idx == 3:
                self.__logger.critical('Timeout for updating package')
                break


    ##
    ## @brief      Gets the cxx compiler.
    ##
    ## @param      self  The object
    ##
    ## @return     The path to the cxx compiler.
    ##
    def get_cxx_compiler(self):
        return None

    ##
    ## @brief      Gets the gcov command.
    ##
    ## @param      self  The object
    ##
    ## @return     The gcov command.
    ##
    def get_gcov_command(self):
        return None

    ##
    ## @brief      Gets the cc compiler.
    ##
    ## @param      self  The object
    ##
    ## @return     The cc compiler.
    ##
    def get_cc_compiler(self):
        return None


    ##
    ## @brief      Gets the additional cmake options necessary to build for android
    ##
    ## @param      self  The object
    ##
    ## @return     The additional cmake options.
    ##
    def get_additional_cmake_options(self):
        cmake_bin_dir = os.path.join(self.__cmake_root, 'bin')

        ndk_root = self.__sdk_root+'/ndk-bundle'
        ninja_exec = os.path.join(cmake_bin_dir,'ninja')
        android_toolchain_file = ndk_root+'/build/cmake/android.toolchain.cmake'
        android_toolchain = re.match('([a-z]+)',self.__compiler_id).group(1)

        cmake_params = [
            '-DANDROID_ABI={0}'.format(self.__target_architecture),
            '-DANDROID_NDK={0}'.format(ndk_root),
            '-DCMAKE_MAKE_PROGRAM={0}'.format(ninja_exec),
            '-DCMAKE_TOOLCHAIN_FILE={0}'.format(android_toolchain_file),
            '-DANDROID_NATIVE_API_LEVEL={0}'.format(self.__native_api_level),
            '-DANDROID_TOOLCHAIN={0}'.format(android_toolchain),
            '-DADNROID_STL={0}'.format(self.__android_stl)
        ]

        return cmake_params

    ##
    ## @brief      Gets the cxx flags.
    ##
    ## @param      self  The object
    ##
    ## @return     The cxx flags.
    ##
    def get_cxx_flags(self):
        return ""

    ##
    ## @brief      Gets the c flags.
    ##
    ## @param      self  The object
    ##
    ## @return     The c flags.
    ##
    def get_c_flags(self):
        return ""

    ##
    ## @brief      Gets the ld flags.
    ##
    ## @param      self  The object
    ##
    ## @return     The ld flags.
    ##
    def get_ld_flags(self):
        return ""

    ##
    ## @brief      Gets the pre configuration commands.
    ##
    ## @param      self  The object
    ##
    ## @return     The pre configuration commands.
    ##
    def get_pre_configuration_commands(self):
        return [], None


    ##
    ## @brief      Gets the default cmake generator.
    ##
    ## @param      self  The object
    ##
    ## @return     The default cmake generator.
    ##
    def get_default_cmake_generator(self):
        return 'Android Gradle - Ninja'