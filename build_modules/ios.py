# -*- coding: utf-8 -*-
#
#  Copyright (c) 2017 Matthias Schmieder
#  This file is part of build-tools (https://bitbucket.org/mschmieder/build-tools).
#
#  This file is licensed to you under your choice of the GNU Lesser
#  General Public License, version 3 or any later version (LGPLv3 or
#  later), or the GNU General Public License, version 2 (GPLv2), in all
#  cases as published by the Free Software Foundation.
#

import shutil
import re
import logging
import os
from .build_utils import *

if sys.platform.startswith("win"):
    import winreg


##
## @brief      Class for ios.
##
class IOS(object):
    def __init__(self, target_architecture,
                       target_system = 'ios',
                       logger = None):
    
        self.__target_architecture = target_architecture
        
        if 'simulator64' in target_system:
            self.__ios_platform = 'SIMULATOR64'
        elif 'simulator' in target_system:
            self.__ios.platform = 'SIMULATOR'
        else:
            self.__ios_platform = 'OS'

        self.__ios_toolchain_file = os.path.join(os.path.dirname(os.path.realpath(__file__)),'..','cmake','iOS.cmake')
        self.__logger = logger


    ##
    ## @brief      Gets the version.
    ##
    ## @param      self  The object
    ##
    ## @return     The version.
    ##
    def get_version(self):
        return re.sub(r'[a-zA-Z-]+', '', self.__compiler_id)


    ##
    ## @brief      Gets the cxx compiler.
    ##
    ## @param      self  The object
    ##
    ## @return     The cxx compiler.
    ##
    def get_cxx_compiler(self):
        candidates = ["/usr/bin/g++"]
        compiler_path = build_utils.find_executable(candidates)

        if compiler_path is None:
            raise Exception("Error! Could not find clang cxx compiler")

        return compiler_path


    ##
    ## @brief      Gets the gcov command.
    ##
    ## @param      self  The object
    ##
    ## @return     The gcov command.
    ##
    def get_gcov_command(self):
       return []


    ##
    ## @brief      Gets the cc compiler.
    ##
    ## @param      self  The object
    ##
    ## @return     The cc compiler.
    ##
    def get_cc_compiler(self):
        candidates = ["/usr/bin/gcc"]

        compiler_path = build_utils.find_executable(candidates)

        if compiler_path is None:
            raise Exception("Error! Could not find clang cc compiler")

        return compiler_path    


    ##
    ## @brief      Gets the default cmake generator.
    ##
    ## @param      self  The object
    ##
    ## @return     The default cmake generator.
    ##
    def get_default_cmake_generator(self):
        return 'Xcode'


    ##
    ## @brief      Gets the additional cmake options.
    ##
    ## @param      self  The object
    ##
    ## @return     The additional cmake options.
    ##
    def get_additional_cmake_options(self):
        opts = []
        if 'arm64' in self.__target_architecture:
            opts.append('-DCMAKE_OSX_ARCHITECTURES=arm64;armv7')
        else:
            opts.append('-DCMAKE_OSX_ARCHITECTURES={0}'.format(self.__target_architecture))
  
        opts.append('-DIOS_PLATFORM={0}'.format(self.__ios_platform))
        opts.append('-DCMAKE_TOOLCHAIN_FILE={0}'.format(self.__ios_toolchain_file))

        return opts


    ##
    ## @brief      Gets the cxx flags.
    ##
    ## @param      self  The object
    ##
    ## @return     The cxx flags.
    ##
    def get_cxx_flags(self):
        return ""


    ##
    ## @brief      Gets the c flags.
    ##
    ## @param      self  The object
    ##
    ## @return     The c flags.
    ##
    def get_c_flags(self):
        return ""


    ##
    ## @brief      Gets the ld flags.
    ##
    ## @param      self  The object
    ##
    ## @return     The ld flags.
    ##
    def get_ld_flags(self):
        return ""


    ##
    ## @brief      Gets the pre configuration commands.
    ##
    ## @param      self  The object
    ##
    ## @return     The pre configuration commands.
    ##
    def get_pre_configuration_commands(self):
        return []
