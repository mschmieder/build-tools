#!/usr/bin/env bash

# -*- coding: utf-8 -*-
#
#  Copyright (c) 2017 Matthias Schmieder
#  This file is part of build-tools (https://bitbucket.org/mschmieder/build-tools).
#
#  This file is licensed to you under your choice of the GNU Lesser
#  General Public License, version 3 or any later version (LGPLv3 or
#  later), or the GNU General Public License, version 2 (GPLv2), in all
#  cases as published by the Free Software Foundation.
#

SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
PROJECT_NAME=$1
OS=$2
shift
shift
ARGS=$@

set -e

if [ -z "${PYTHON3}" ]; then
  PYTHON3=python3
fi


# check if there is a path file that needs to be considered
filename="travis_env_paths"
if [ -f "${filename}" ]; then
    while read -r line
    do
        export PATH="$line:${PATH}"
        echo "Add to PATH: '${line}'"
    done < "${filename}"
fi

case $OS in
docker-*)
    CPP_BASE_BUILDSYSTEM_TAG=$(echo $OS | sed 's/docker-//g') 
    ${PYTHON3} ${SCRIPT_DIR}/../../cmakew_docker --container-name ${PROJECT_NAME} --docker-image-tag ${CPP_BASE_BUILDSYSTEM_TAG} ${ARGS}
    exit $?
    ;;
macos | linux | windows | native )
    ${PYTHON3} ${SCRIPT_DIR}/../../cmakew ${ARGS}
    exit $?
    ;;
*)
    echo "Unsupported OS: $OS"
    exit 1
esac
