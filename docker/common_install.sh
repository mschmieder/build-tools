#!/bin/bash

set -e

apt-get update -qq
apt-get install -y --no-install-recommends wget

# 1E9377A2BA9EF27F is the key for the ubuntu-toolchain-r PPA.
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 1E9377A2BA9EF27F | cat

apt-get update -qq
apt-get install -y --allow-unauthenticated --no-install-recommends \
    file \
    valgrind \
    make \
    cmake \
    libboost-dev \
    gcc \
    g++ \
    gcc-5 \
    gcc-6 \
    g++-5 \
    g++-6 \
    clang \
    clang-4.0 \
    llvm-4.0 \
    clang-5.0 \
    llvm-5.0 \
    clang-6.0 \
    llvm-6.0 \
    perl \
    cppcheck \
    cccc \
    libc++-dev \
    libgl1-mesa-dev \
    mesa-common-dev \
    freeglut3-dev \
    python3-pytest \
    python3-pip \
    python3-setuptools \
    dirmngr
    
pip3 install --upgrade pip
pip3 install wheel
pip3 install pytest-xdist
