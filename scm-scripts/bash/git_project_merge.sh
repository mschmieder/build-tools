#!/usr/bin/env bash
#Set fonts for Help.
set +e
if [ `which tput` ];then
  NORM=`tput sgr0`
  BOLD=`tput bold`
  REV=`tput smso`
fi


################################################################################
# HELPER
################################################################################
#Help function
function HELP {
  echo -e \\n"Help documentation for ${BOLD}${SCRIPT}.${NORM}"\\n
  echo -e "${BOLD}-c${NORM},${BOLD}--git-commit ${NORM}\n\t git commit id that needs merging"
  echo -e "${BOLD}-m${NORM},${BOLD}--git-merge-branch ${NORM}\n\t branch name in which the commit passed with --git-commit will be merged into"
  echo -e "${BOLD}-u${NORM},${BOLD}--git-url ${NORM}\n\t repository url"
  echo -e "${BOLD}--commit-message ${NORM}(optional) \n\t optional commit message to use for merging"
  exit 0
}

declare -a arr_merge_branches

if [ $# -lt 1 ]; then
    HELP
fi

################################################################################
# OPTIONS
################################################################################
OPTS=$(getopt -o c:hm:u: --long help,git-commit:,git-url:,git-merge-branch:,commit-message: -- "$@")

if [ $? != 0 ]
then
    exit 1
fi

eval set -- "$OPTS"

while true; do
  case "$1" in
    --commit-message )
      COMMIT_MESSAGE="${2}"
      shift 2;;
    -c | --git-commit)
      GIT_COMMIT="${2}"
      shift 2;;
    -m | --git-merge-branch)
      arr_merge_branches+=("${2}")
      shift 2;;
    -u | --git-url)
      GIT_URL+="${2}"
      shift 2;;
    -h | --help ) HELP; exit 0;;
    --) shift; break;;
    *) echo "Internal error!"; exit 1;;
  esac
done

BUILD_TYPE=${BUILD_TYPE:='release'}

# get project name from git url
_project_name_we=${GIT_URL##*/}
_name_=${_project_name_we%.*}


echo "${BOLD}-------------------------------------------------------------------"
echo "PROJECT MERGE CONFIGURATION:"
echo "-------------------------------------------------------------------${NORM}"
echo "${BOLD}--> project info:${NORM}"
echo "-------------------------------------------------------------------"
echo -e "\t*  project-name:        ${BOLD} ${_name_} ${NORM}"
echo -e "\t*  git-url:             ${BOLD} ${GIT_URL} ${NORM}"
echo -e "\t*  git-commit:          ${BOLD} ${GIT_COMMIT} ${NORM}"
echo -e "\t*  merge-targets:       ${BOLD} ${arr_merge_branches[@]} ${NORM}"
echo "-------------------------------------------------------------------"

RETCODE=0
RETCODE_=0

# split branches if they are joined by ';'
declare -a merge_branches
for branch in "${arr_merge_branches[@]}"
do
  merge_branches+=($(echo $branch | tr ";" "\n"))
done

# clone repository
echo "${BOLD}--> cloning ${GIT_URL}${NORM}"
if [ -d "$(pwd)/${_name_}" ]; then
echo "    already present...skipping"
else
  git clone ${GIT_URL} 
fi

# iterate over all branches and merge them
declare -A map_merge_result
for branch in "${merge_branches[@]}"
do
  map_merge_result["$branch"]="success"
  echo "${BOLD}-->   exec: git checkout $branch${NORM}"
  echo git checkout $branch   
  echo "${BOLD}-->   exec: git merge $GIT_COMMIT${NORM}"
  if [ -z ${COMMIT_MESSAGE} ];then
    echo git merge $GIT_COMMIT -m "auto-merge ${GIT_COMMIT} into ${branch}"
  else
    echo git merge $GIT_COMMIT -m "${COMMIT_MESSAGE}"
  fi

  if [ $? -ne 0 ];then
    echo "${BOLD}-->   ERROR: auto-merging failed! Please merge manually${NORM}"
    echo git reset --hard HEAD
    map_merge_result["${branch}"]="merge failed"
  fi

  echo "${BOLD}-->   pushing merged branch back to origin${NORM}"
  echo "${BOLD}-->     exec: git push origin $branch${NORM}"
  echo git push origin $branch

  if [ $? -ne 0 ];then
    echo "${BOLD}-->   ERROR: auto-merging failed! Please merge manually"
    echo git reset --hard HEAD
    map_merge_result["${branch}"]="push failed"
  fi

done

_retcode_=0
for i in "${!array[@]}"
do
  echo "key  : $i"
  echo "value: ${array[$i]}"
done

echo "${BOLD}-------------------------------------------------------------------"
echo "PROJECT MERGE RESULTS:"
echo "-------------------------------------------------------------------${NORM}"
for i in "${!map_merge_result[@]}"
do
  echo -e "\t*  merge ${BOLD}${GIT_COMMIT}${NORM} into ${BOLD}${i}${NORM}:     ${BOLD} ${map_merge_result[$i]} ${NORM}"

  if [[ "${map_merge_result[$i]}" == *failed* ]];then
    _retcode_=1
  fi

done
echo "${BOLD}-------------------------------------------------------------------${NORM}"

exit $_retcode_
