# === Authors
#
# Matthias Schmieder <schmieder.matthias@gmail.com>
#
# === Copyright
#
# Copyright 2016 Matthias Schmieder

#!/usr/bin/env bash

SCRIPT=`basename ${BASH_SOURCE[0]}`

_host_system_uname_=$(uname)
if [[ ${_host_system_uname_} == *MINGW* ]];then 
  HOST_SYSTEM="windows"
elif [[ ${_host_system_uname_} == *CYGWIN* ]]; then
  HOST_SYSTEM="windows"
  CYGWIN=true
elif [[ ${_host_system_uname_} == *Linux* ]]; then
  HOST_SYSTEM="linux"
else # darwin
  HOST_SYSTEM="macos"
fi

#Set fonts for Help.
set +e
if [ `which tput` ];then
  NORM=`tput sgr0`
  BOLD=`tput bold`
  REV=`tput smso`
fi
set -e

if [ $(which mvn) ];then
  MAVEN_EXEC=$(which mvn)
elif [ $(which mvn3) ];then
  MAVEN_EXEC=$(which mvn3)
elif [ $(which mvn2) ];then
  MAVEN_EXEC=$(which mvn2)
elif [ $(which mvn1) ];then
  MAVEN_EXEC=$(which mvn1)
fi

################################################################################
# HELPER
################################################################################
#Help function
function HELP {
  echo -e \\n"Help documentation for ${BOLD}${SCRIPT}${NORM}"\\n 
  echo -e "${BOLD}-f${NORM},${BOLD}--force ${NORM}\n\t will download and overwrite artifacts even if they are already installed"
  echo -e "${BOLD}-g${NORM},${BOLD}--artifact-group ${NORM}\n\t group where to find the artifact"
  echo -e "${BOLD}-i${NORM},${BOLD}--install-directory(optional) ${NORM}\n\t directory where to install the artifact"
  echo -e "${BOLD}-m${NORM},${BOLD}--maven-home(optional) ${NORM}\n\t maven home directory"
  echo -e "${BOLD}-n${NORM},${BOLD}--artifact-name ${NORM}\n\t name of the artifact"
  echo -e "${BOLD}-p${NORM},${BOLD}--artifact-packaging ${NORM}\n\t name of the artifact"
  echo -e "${BOLD}-u${NORM},${BOLD}--repo-url ${NORM}\n\t url where to find the maven repository"
  echo -e "${BOLD}-r${NORM},${BOLD}--repo-id ${NORM}\n\t id of the repository"
  echo -e "${BOLD}-v${NORM},${BOLD}--artifact-version ${NORM}\n\t required version of the artifact" 
  echo -e "${BOLD}--verbose ${NORM}\n\t verbose output" 
  exit 0
}

if [ $# -lt 1 ]; then
    HELP
fi

################################################################################
# OPTIONS
################################################################################
OPTS=$(getopt -o fg:hi:m:n:p:r:u:v: --long artifact-name:,artifact-group:,artifact-version:,artifact-packaging:,force,help,install-directory:,repo-url:,repo-id,maven-home:,verbose -- "$@")

if [ $? != 0 ]
then
    exit 1
fi

eval set -- "$OPTS"

while true; do
  case "$1" in
    -f | --force)
      FORCE=true
      shift 1;;
    -g | --artifact-group)
      ARTIFACT_GROUP="$2"
      shift 2;;
    -i | --install-directory)
      INSTALL_DIRECTORY="$2"
      shift 2;;
    -m | --maven-home)
      MAVEN_HOME="$2"
      shift 2;;
    -n | --artifact-name)
      ARTIFACT_NAME="$2"
      shift 2;;
    -p | --artifact-packaging)
      ARTIFACT_PACKAGING="$2"
      shift 2;;
    -r | --repo-id)
      REPO_ID="$2"
      shift 2;;
    -u | --repo-url)
      REPO_URL="$2"
      shift 2;;
    -v | --artifact-version)
      ARTIFACT_VERSION="$2"
      shift 2;;
    --verbose)
      VERBOSE=true
      shift 1;;
    -h | --help ) HELP; exit 0;;
    --) shift; break;;
    *) echo "Internal error!"; exit 1;;
  esac
done

MAVEN_HOME=${MAVEN_HOME:="$HOME/.m2"}
REPO_ID=${REPO_ID:="nexus"}
INSTALL_DIRECTORY=${INSTALL_DIRECTORY:="$(pwd)"}
ARTIFACT_PACKAGING=${ARTIFACT_PACKAGING:="zip"}
VERBOSE=${VERBOSE:=false}
FORCE=${FORCE:=false}

if [ -z "${ARTIFACT_NAME}" ];then
  echo "Error: missing artifact-name."
  HELP
  exit 1
fi

if [ -z "${ARTIFACT_VERSION}" ];then
  echo "Error: missing artifact-version."
  HELP
  exit 1
fi

if [ -z "${ARTIFACT_GROUP}" ];then
  echo "Error: missing artifact-group."
  HELP
  exit 1
fi

if [ -z "${REPO_URL}" ];then
  echo "Error: missing repo-url."
  HELP
  exit 1
fi

if [ -n "${CYGWIN}" ];then
  MAVEN_HOME=$(echo ${MAVEN_HOME} | sed -e 's,/home/\([a-zA-Z]\),C:/Users/\1,g' )
fi

local_repository="${MAVEN_HOME}/repository/$(echo ${ARTIFACT_GROUP} | tr '.' '/' )/${ARTIFACT_NAME}/${ARTIFACT_VERSION}"
local_artifact=${local_repository}/${ARTIFACT_NAME}-${ARTIFACT_VERSION}.${ARTIFACT_PACKAGING}

echo "${BOLD}-------------------------------------------------------------------"
echo "MAVEN DOWNLOAD CONFIGURATION:"
echo "-------------------------------------------------------------------${NORM}"
echo -e "\t*  maven-binary:            ${BOLD} ${MAVEN_EXEC} ${NORM}"
echo -e "\t*  maven-home:              ${BOLD} ${MAVEN_HOME} ${NORM}"
echo -e "\t*  repo-url:                ${BOLD} ${REPO_URL} ${NORM}"
echo -e "\t*  repo-id:                 ${BOLD} ${REPO_ID} ${NORM}"
echo -e "\t*  artifact-packaging:      ${BOLD} ${ARTIFACT_PACKAGING} ${NORM}"
echo -e "\t*  artifact-name:           ${BOLD} ${ARTIFACT_NAME} ${NORM}"
echo -e "\t*  artifact-version:        ${BOLD} ${ARTIFACT_VERSION} ${NORM}"
echo -e "\t*  artifact-group:          ${BOLD} ${ARTIFACT_GROUP} ${NORM}"
echo -e "\t*  local-repository:        ${BOLD} ${local_repository} ${NORM}"
echo -e "\t*  local-artifact:          ${BOLD} ${local_artifact} ${NORM}"
echo -e "\t*  install-directory:       ${BOLD} ${INSTALL_DIRECTORY}/${ARTIFACT_NAME}/${ARTIFACT_VERSION} ${NORM}"
echo -e "\t*  force overwrite:         ${BOLD} ${FORCE} ${NORM}"
echo "-------------------------------------------------------------------"
if [ -d "${INSTALL_DIRECTORY}/${ARTIFACT_NAME}/${ARTIFACT_VERSION}" ] && [ "${FORCE}" == "false" ];then 
  echo "${BOLD}${ARTIFACT_NAME} already installed. Skipping download...${NORM}"
  exit 0
fi

_cmd="${MAVEN_EXEC} org.apache.maven.plugins:maven-dependency-plugin:2.1:get \
                    -DrepoUrl=${REPO_URL} \
                    -Dpackaging=${ARTIFACT_PACKAGING} \
                    -Dartifact=${ARTIFACT_GROUP}:${ARTIFACT_NAME}:${ARTIFACT_VERSION} \
                    -DrepoId=${REPO_ID}"

if [ ${VERBOSE} = false ];then
  ${_cmd} > /dev/null
else
  ${_cmd}
fi

if [ -n "${CYGWIN}" ];then
  local_artifact=$(echo ${local_artifact} | tr '\\' '/' | sed -e 's,\([a-zA-Z]\):,/cygdrive/\1,g' )
  INSTALL_DIRECTORY=$(echo ${INSTALL_DIRECTORY} | tr '\\' '/' | sed -e 's,\([a-zA-Z]\):,/cygdrive/\1,g' )
fi

if [ -f "${local_artifact}" ];then
  mkdir -p ${INSTALL_DIRECTORY}/${ARTIFACT_NAME}/${ARTIFACT_VERSION}
  if [ "${ARTIFACT_PACKAGING}" = "zip" ];then
    unzip -o ${local_artifact} -d ${INSTALL_DIRECTORY}/${ARTIFACT_NAME}/${ARTIFACT_VERSION} > /dev/null
  else
    cp ${local_artifact} ${INSTALL_DIRECTORY}/${ARTIFACT_NAME}/${ARTIFACT_VERSION}/
  fi
else
  echo "Error: ${local_artifact} missing!"
  exit 1
fi

