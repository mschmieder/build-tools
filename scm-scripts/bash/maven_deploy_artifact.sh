# === Authors
#
# Matthias Schmieder <schmieder.matthias@gmail.com>
#
# === Copyright
#
# Copyright 2016 Matthias Schmieder

#!/usr/bin/env bash

SCRIPT=`basename ${BASH_SOURCE[0]}`
#Set fonts for Help.
set +e
if [ `which tput` ];then
  NORM=`tput sgr0`
  BOLD=`tput bold`
  REV=`tput smso`
fi
set -e

if [ $(which mvn) ];then
  MAVEN_EXEC=$(which mvn)
elif [ $(which mvn3) ];then
  MAVEN_EXEC=$(which mvn3)
elif [ $(which mvn2) ];then
  MAVEN_EXEC=$(which mvn2)
elif [ $(which mvn1) ];then
  MAVEN_EXEC=$(which mvn1)
fi

################################################################################
# HELPER
################################################################################
#Help function
function HELP {
  echo -e \\n"Help documentation for ${BOLD}${SCRIPT}${NORM}"\\n 
  echo -e "${BOLD}-g${NORM},${BOLD}--artifact-group ${NORM}\n\t group where to find the artifact"
  echo -e "${BOLD}-f${NORM},${BOLD}--artifact-file ${NORM}\n\t file path of the artifact to deploy"
  echo -e "${BOLD}-n${NORM},${BOLD}--artifact-id ${NORM}\n\t name of the artifact"
  echo -e "${BOLD}-p${NORM},${BOLD}--artifact-packaging ${NORM}\n\t name of the artifact"
  echo -e "${BOLD}-u${NORM},${BOLD}--repo-url ${NORM}\n\t url where to find the maven repository"
  echo -e "${BOLD}-r${NORM},${BOLD}--repo-id ${NORM}\n\t repository id"
  echo -e "${BOLD}-v${NORM},${BOLD}--artifact-version ${NORM}\n\t required version of the artifact" 
  echo -e "${BOLD}--verbose ${NORM}\n\t verbose output" 
  exit 0
}

if [ $# -lt 1 ]; then
    HELP
fi

################################################################################
# OPTIONS
################################################################################
OPTS=$(getopt -o f:g:hm:n:p:r:u:v: --long artifact-id:,artifact-file:,artifact-group:,artifact-version:,artifact-packaging:,help,repo-url:,repo-id:,verbose -- "$@")

if [ $? != 0 ]
then
    exit 1
fi

eval set -- "$OPTS"

while true; do
  case "$1" in
    -f | --artifact-file)
      ARTIFACT_FILE="$2"
      shift 2;;
    -g | --artifact-group)
      ARTIFACT_GROUP="$2"
      shift 2;;
    -n | --artifact-id)
      ARTIFACT_ID="$2"
      shift 2;;
    -p | --artifact-packaging)
      ARTIFACT_PACKAGING="$2"
      shift 2;;
    -u | --repo-url)
      REPOSITORY_URL="$2"
      shift 2;;
    -r | --repo-id)
      REPOSITORY_ID="$2"
      shift 2;;
    -v | --artifact-version)
      ARTIFACT_VERSION="$2"
      shift 2;;
    --verbose)
      VERBOSE=true
      shift 1;;
    -h | --help ) HELP; exit 0;;
    --) shift; break;;
    *) echo "Internal error!"; exit 1;;
  esac
done

MAVEN_HOME=${MAVEN_HOME:="$HOME/.m2"}
INSTALL_DIRECTORY=${INSTALL_DIRECTORY:="$(pwd)"}
ARTIFACT_PACKAGING=${ARTIFACT_PACKAGING:="zip"}
VERBOSE=${VERBOSE:=false}

if [ -z "${ARTIFACT_ID}" ];then
  echo "Error: missing artifact-id."
  HELP
  exit 1
fi

if [ -z "${ARTIFACT_VERSION}" ];then
  echo "Error: missing artifact-version."
  HELP
  exit 1
fi

if [ -z "${ARTIFACT_GROUP}" ];then
  echo "Error: missing artifact-group."
  HELP
  exit 1
fi

if [ -z "${REPOSITORY_URL}" ];then
  echo "Error: missing repo-url."
  HELP
  exit 1
fi

if [ -z "${REPOSITORY_ID}" ];then
  echo "Error: missing repo-id."
  HELP
  exit 1
fi

if [ -z "${ARTIFACT_FILE}" ];then
  echo "Error: missing artofact-file."
  HELP
  exit 1
fi

echo "${BOLD}-------------------------------------------------------------------"
echo "MAVEN DOWNLOAD CONFIGURATION:"
echo "-------------------------------------------------------------------${NORM}"
echo -e "\t*  maven-binary:            ${BOLD} ${MAVEN_EXEC} ${NORM}"
echo -e "\t*  repository-id:           ${BOLD} ${REPOSITORY_ID} ${NORM}"
echo -e "\t*  repository-url:          ${BOLD} ${REPOSITORY_URL} ${NORM}"
echo -e "\t*  artifact-packaging:      ${BOLD} ${ARTIFACT_PACKAGING} ${NORM}"
echo -e "\t*  artifact-id:             ${BOLD} ${ARTIFACT_ID} ${NORM}"
echo -e "\t*  artifact-version:        ${BOLD} ${ARTIFACT_VERSION} ${NORM}"
echo -e "\t*  artifact-group:          ${BOLD} ${ARTIFACT_GROUP} ${NORM}"
echo -e "\t*  artifact-file:           ${BOLD} ${ARTIFACT_FILE} ${NORM}"
echo "-------------------------------------------------------------------"

_cmd="${MAVEN_EXEC} deploy:deploy-file \
                    -DgroupId=${ARTIFACT_GROUP} \
                    -DartifactId=${ARTIFACT_ID} \
                    -Dversion=${ARTIFACT_VERSION} \
                    -Dpackaging=${ARTIFACT_PACKAGING} \
                    -Dfile=${ARTIFACT_FILE} \
                    -DrepositoryId=${REPOSITORY_ID} \
                    -DgeneratePom=true \
                    -Durl=${REPOSITORY_URL}" 

if [ ${VERBOSE} = false ];then
  ${_cmd} > /dev/null
else
  ${_cmd}
fi

