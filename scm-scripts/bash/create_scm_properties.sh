#!/usr/bin/env bash
#Set fonts for Help.
set +e
if [ `which tput` ];then
  NORM=`tput sgr0`
  BOLD=`tput bold`
  REV=`tput smso`
fi


################################################################################
# HELPER
################################################################################
#Help function
function HELP {
  echo -e \\n"Help documentation for ${BOLD}${SCRIPT}.${NORM}"\\n
  echo -e "${BOLD}-o${NORM},${BOLD}--output ${NORM}\n\t output file"
  echo -e "${BOLD}-d${NORM},${BOLD}--git-dir ${NORM} (optional) \n\t directory of the git project (default: cwd)"
  echo -e "${BOLD}-p${NORM},${BOLD}--param ${NORM} (optional) \n\t additional parameter to store (KEY:Value)"
  echo -e "${BOLD}--prefix ${NORM} (optional) \n\t adds an prefix to the variables that are written to the file"
  echo -e "${BOLD}--crawl ${NORM} (optional) \n\t will check each folder if it is a git repository"
  echo -e "${BOLD}--exclude-pattern ${NORM} (optional) \n\t exclude pattern that will be used if crawl is activated"
  exit 0
}

declare -a arr_merge_branches
declare -a arr_exclude_pattern

if [ $# -lt 1 ]; then
    HELP
fi

declare -A map_params

################################################################################
# OPTIONS
################################################################################
OPTS=$(getopt -o d:ho:p: --long crawl,help,output:,git-dir:,param:,prefix:,exclude-pattern: -- "$@")

if [ $? != 0 ]
then
    exit 1
fi

eval set -- "$OPTS"

while true; do
  case "$1" in
    --crawl )
      CRAWL=true
      shift 1;;
    --exclude-pattern )
      arr_exclude_pattern+=("$2")
      shift 2;;
    -d | --git-dir )
      GIT_DIR="${2}"
      shift 2;;
    -o | --output)
      OUTPUT_FILE="${2}"
      shift 2;;
    -p | --param)
      KEY=$(echo $2 | cut -d':' -f1)
      VALUE=$(echo $2 | cut -d':' -f2)
      map_params["$KEY"]="${VALUE}"
      shift 2;;
    -p | --prefix)
        PREFIX="${2}"
        shift 2;;
    -h | --help ) HELP; exit 0;;
    --) shift; break;;
    *) echo "Internal error!"; exit 1;;
  esac
done

if [ -z "${OUTPUT_FILE}" ];then
  echo "Error: output parameter not set."
  HELP
  exit 1
fi

if [ -n "${PREFIX}" ];then
  PREFIX="${PREFIX}_"
fi

GIT_DIR=${GIT_DIR:="$(pwd)"}
CRAWL=${CRAWL:=false}
rm -f ${OUTPUT_FILE}

_cwd_=$(pwd)
if [ ${CRAWL} = true ];then
  arr_dirs=($(ls -d ${GIT_DIR}/*/))
  for dir in "${arr_dirs[@]}"
  do
    if [ -d "${dir}/.git" ];then

      _exclude_hit_=false
      for pattern in "${arr_exclude_pattern[@]}" 
      do
        if [ $(echo ${dir} | grep -E "${pattern}") ];then
          _exclude_hit_=true    
          break
        fi
      done

      if [ ${_exclude_hit_} = false ];then
        cd ${dir}
        _project_name_=$(basename ${dir})
        PREFIX=$(echo ${_project_name_}_ |  sed 's/-/_/' | awk '{print toupper($0)}' )

        GIT_COMMIT=$(git rev-parse HEAD)
        GIT_URL=$(git config --get remote.origin.url)

        echo "${PREFIX}GIT_COMMIT=${GIT_COMMIT}" >> ${OUTPUT_FILE}
        echo "${PREFIX}GIT_URL=${GIT_URL}"       >> ${OUTPUT_FILE}  
      fi

    fi
  done
else
  # get git properties
  cd ${GIT_DIR}
  GIT_COMMIT=$(git rev-parse HEAD)
  GIT_URL=$(git config --get remote.origin.url)

  echo "${PREFIX}GIT_COMMIT=${GIT_COMMIT}" >> ${OUTPUT_FILE}
  echo "${PREFIX}GIT_URL=${GIT_URL}"       >> ${OUTPUT_FILE}  
fi

cd ${_cwd_}
# add parameters to file
for i in "${!map_params[@]}"
do
  echo "${PREFIX}$i=${map_params[$i]}">>${OUTPUT_FILE}
done